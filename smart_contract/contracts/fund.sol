//SPDX-License-Identifier: Unlicense

pragma solidity ^0.8.0;

contract fundManager
{
    event createFundEvent(address indexed creator, uint goalAmount, string message);
    event fundFundedEvent(address indexed owner, uint goalAmount, string message);
    event contributedFundEvent(address owner, address contributor, uint contributedAmount, uint goalAmount, string message);

    uint private constant MAX_FUNDS_PER_GROUP = 5;

    struct fund
    {
        address owner;
        string message;
        uint goalAmount;
        uint treasury;
        bool funded;
        address[] contributors;
    }

    struct group
    {
        uint fundsNum;
        mapping(address => fund) funds;
        address[MAX_FUNDS_PER_GROUP] funders;
        mapping(address => uint) adrNum;
    }

    function addFundToGroup(fund memory newFund, group storage gr) private
    {
        require(gr.fundsNum <= MAX_FUNDS_PER_GROUP, "Cannot create more funds for this group");
        gr.funds[newFund.owner] = newFund;
        for(uint i = 0; i < MAX_FUNDS_PER_GROUP; i++)
        {
            if(gr.funders[i] == address(0x0))
            {
                gr.funders[i] = newFund.owner;
                gr.fundsNum++;
                gr.adrNum[newFund.owner] = i;
                return;
            }
        }
    }

    function setFund(address _owner, string memory _message, uint _goalAmount, uint _treasury) private pure returns(fund memory)
    {
        fund memory newFund;
        newFund.owner = _owner;
        newFund.message = _message;
        newFund.goalAmount = _goalAmount;
        newFund.treasury = _treasury;
        newFund.funded = false;
        return newFund;
    }

    mapping(string => group) private groupFunds;

    function createFund(uint256 goalAmount, string memory message, string memory groupId) external payable
    {
        require(goalAmount > 0, "Invalid goal amount");
        require(groupFunds[groupId].fundsNum <= MAX_FUNDS_PER_GROUP, "Maximum number of active funds campaigns exceeded");
        bool validOwner = groupFunds[groupId].funds[msg.sender].owner == address(0x0);
        bool isFunded = groupFunds[groupId].funds[msg.sender].funded == true;
        require(validOwner || isFunded, "Cannot have more than one active fund campaign");
        require(msg.value < goalAmount, "No point in creating fund if you already have the money needed");
        fund memory f = setFund(msg.sender, message, goalAmount, msg.value);
        addFundToGroup(f, groupFunds[groupId]);
        emit createFundEvent(msg.sender, goalAmount, message);
    }

    function withdrawFund(string memory groupId) external
    {
        fund storage f = groupFunds[groupId].funds[msg.sender];
        require(f.owner != address(0x0), "No fund campaign to withdraw from");
        withdrawFundPrivate(f, groupFunds[groupId]);
    }

    function withdrawFundPrivate(fund storage f, group storage groupF) private
    {
        payable(f.owner).transfer(f.treasury);
        f.funded = true;
        uint arrIndex = groupF.adrNum[f.owner];
        delete groupF.funders[arrIndex];
        groupF.fundsNum--;
        emit fundFundedEvent(f.owner, f.goalAmount, f.message);
    }

    function contributeToFund(address fundOwner, string memory groupId) external payable returns(uint)
    {
        require(msg.value > 0, "Enter valid amount");
        fund storage f = groupFunds[groupId].funds[fundOwner];
        require(f.owner != address(0x0), "No such fund exists");
        require(f.funded == false, "Already funded");
        f.treasury += msg.value;
        f.contributors.push(msg.sender);
        if(f.treasury >= f.goalAmount)
        {
            withdrawFundPrivate(f, groupFunds[groupId]);
        }
        emit contributedFundEvent(f.owner, msg.sender, msg.value, f.goalAmount, f.message);
        return msg.value;
    }

    function getFund(string memory groupId, address fundOwner) external view returns(fund memory)
    {
        return groupFunds[groupId].funds[fundOwner];
    }

    function getActiveGroupFunds(string memory groupId) external view returns(fund[] memory)
    {
        uint size = groupFunds[groupId].fundsNum;
        uint j = 0;
        fund[] memory grfunds = new fund[](size);
        for(uint i = 0; i < MAX_FUNDS_PER_GROUP; i++)
        {
            address adr = groupFunds[groupId].funders[i];
            if(adr != address(0x0))
            {
                grfunds[j] = groupFunds[groupId].funds[adr];
                j++;
                if(j == size) break;
            }
        }
        return grfunds;
    }
}