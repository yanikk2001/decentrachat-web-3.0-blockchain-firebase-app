export function showAlert(message)
{
    document.querySelector('.alert-message').innerHTML = `${message}`;
    document.querySelector('.alert').style.display = 'grid';
}

export function hideAlert()
{
    document.querySelector('.alert').style.display = 'none';
}