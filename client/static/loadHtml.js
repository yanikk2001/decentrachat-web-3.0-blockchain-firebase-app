export async function loadHtml(page)
{
    var pageElement = document.querySelector('#page');

    // !!!!!!!!!!!! Cached page sometimes causes problems should look into it
    const cachedPage = await sessionStorage.getItem(page);//If it was already loaded it will be stored in the session cache
    if(cachedPage != null)
    {
        var result = cachedPage;
        console.log("cached");

        pageElement.innerHTML = result;
        //Create events, when it is dispatched and listened to can add more custom logic.
        // Exampe when login.html is loaded add onclick listener on login form
        const event = new Event(page); //Emit event notifying that it was loaded from cache
        document.dispatchEvent(event); 
    }
    else
    {
        console.log("not cached");

        var response = await fetch("templates/" + page);
        var result = await response.text();
        if(!response.ok)
        {
            result = `<h1 class="black"> ${response.statusText} </h1>`;
        }
        else
        {
            sessionStorage.setItem(page, result);
        }

        pageElement.innerHTML = result;
        // const event = new Event(page + '-loaded'); ////Emit event notifying that it was loaded from the server
        const event = new Event(page); ////Emit event notifying that it was loaded from the server
        document.dispatchEvent(event); 
    }
}