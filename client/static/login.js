import {loadHtml} from "./loadHtml.js";
import { getAuth, signInWithEmailAndPassword } from "firebase/auth";

function showAlert(message)
{
    document.querySelector('.alert-message').innerHTML = `${message}`;
    document.querySelector('.alert').style.display = 'grid';
}

function hideAlert()
{
    document.querySelector('.alert').style.display = 'none';
}

document.addEventListener('log-in.html', () => {
    document.getElementById('signIn').addEventListener('click', () => loadHtml('sign-in.html'));

    document.querySelector('.login-form').onsubmit = () => {
        hideAlert();

        let email = document.querySelector('input[type=email]');
        let password = document.querySelector('input[type=password]');

        const auth = getAuth();
        signInWithEmailAndPassword(auth, email.value, password.value)
        .then((userCredential) => {
            // Signed in 
            const user = userCredential.user;
            // window.location.href = "index.html";
            loadHtml('home.html');
        })
        .catch((error) => {
            const errorMessage = error.message;
            showAlert(errorMessage);
        });
        
        return false;
    }
});

