import { getAuth, createUserWithEmailAndPassword } from "firebase/auth";
import {loadHtml} from "./loadHtml.js"

    function showAlert(message)
    {
        document.querySelector('.alert-message').innerHTML = `${message}`;
        document.querySelector('.alert').style.display = 'grid';
    }

    function hideAlert()
    {
        document.querySelector('.alert').style.display = 'none';
    }

    document.addEventListener('sign-in.html', () => {
        document.getElementById('logIn').addEventListener('click', () => loadHtml('log-in.html'));

        document.querySelector('#signin-form').onsubmit = () => {
            hideAlert();

            let email = document.querySelector('input#form-signin-email');
            let password = document.querySelector('input#form-signin-password');
            let retypePassWord = document.querySelector('input#form-signin-retypepass');

            if(password.value != retypePassWord.value)
            {
                showAlert('Passwords do not match!');
                password.value = '';
                retypePassWord.value = '';
            }
            else
            {
                const auth = getAuth();
                createUserWithEmailAndPassword(auth, email.value, password.value)
                .then((userCredential) => {
                    // Signed in 
                    const user = userCredential.user;
                    // window.location.href = "index.html";
                    loadHtml('home.html');
                })
                .catch((error) => {
                    const errorCode = error.code;
                    const errorMessage = error.message;
                    showAlert(errorMessage);
                });
            }
            return false;
        }
    });