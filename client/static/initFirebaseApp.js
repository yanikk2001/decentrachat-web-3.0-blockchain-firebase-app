// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";

// import { getAnalytics } from "https://www.gstatic.com/firebasejs/9.6.6/firebase-analytics.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyDG87iKxiaR11XP3Hj-qvDEhXb2SqZK07A",
    authDomain: "web-3-chat-app.firebaseapp.com",
    databaseURL: "https://web-3-chat-app-default-rtdb.europe-west1.firebasedatabase.app/",
    projectId: "web-3-chat-app",
    storageBucket: "web-3-chat-app.appspot.com",
    messagingSenderId: "451032577021",
    appId: "1:451032577021:web:aef80cc84b22504f691f94",
    measurementId: "G-GMPRRVLP8J"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
// const analytics = getAnalytics(app);