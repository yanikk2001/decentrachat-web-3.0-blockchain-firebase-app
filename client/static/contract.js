import { ethers } from "ethers";
import fundManager from '../../smart_contract/src/artifacts/contracts/fund.sol/fundManager.json';

const contractAddress = '0x5FbDB2315678afecb367f032d93F642f64180aa3'; //Contract address

// request access to the user's MetaMask account
export async function requestAccount() {
    await window.ethereum.request({ method: 'eth_requestAccounts' });
}

export async function walletIsConnected(){
    if(window.ethereum !== 'undefined'){
        let provider = new ethers.providers.Web3Provider(window.ethereum);
        const accounts = await provider.listAccounts();
        return accounts.length != 0;
    }
}

export function getActiveAddress(){
    let provider = new ethers.providers.Web3Provider(window.ethereum);
    let signer = provider.getSigner();
    return signer.getAddress();
}

export function shortenAddress(address){
    let ownerStr = address.toString();
    return ownerStr.substring(0, 5) + '...' + ownerStr.substring(ownerStr.length - 4, ownerStr.length);
}

export class FundManager {
    constructor() {
        this.provider = new ethers.providers.Web3Provider(window.ethereum);
        this.contract = new ethers.Contract(contractAddress, fundManager.abi, this.provider);

        this.contract.on("contributedFundEvent", (owner, contributor, contributedAmount, goalAmount, message) => {
            console.log(`${contributor} contributed to ${message} by ${owner} with ${ethers.utils.formatEther(contributedAmount)} ETH!`);
            const event = new Event("contributedFundEvent");
            document.dispatchEvent(event);
        });

        this.contract.on("createFundEvent", (creator, goalAmount, message) => {
            console.log(`${creator} just created a new fund campaign ${message} with a goal of ${ethers.utils.formatEther(goalAmount)} ETH`);
            const event = new Event("createFundEvent");
            document.dispatchEvent(event);
        });

        this.contract.on("fundFundedEvent", (owner, goalAmount, message) => {
            console.log(`Fund ${message} by ${owner} has reached its goal amount of ${ethers.utils.formatEther(goalAmount)}`);
            const event = new Event("fundFundedEvent");
            document.dispatchEvent(event);
        });
    }

    #connectSigner(){
        this.signer = this.provider.getSigner();
        this.contract = new ethers.Contract(contractAddress, fundManager.abi, this.signer);
    }

    getSigner(){
        this.signer = this.provider.getSigner();
        return this.signer.getAddress();
    }

    async startFundCampaign(goalAmount, message, groupId, startAmount){
        await requestAccount();
        this.#connectSigner();

        if (typeof window.ethereum !== 'undefined') {
            if(message.length > 25) throw("Fund title should be max 25 characters long");
            if(message === '' || goalAmount === '') {
                throw('First 2 fields are required');
            }
            if(goalAmount <= 0)
            {
                throw('Invalid goal number');
            }
            if(startAmount < 0){
                throw('Invalid starting amount, should be >= 0');
            }
            if(startAmount === '') startAmount = 0;

            try{
                const status = await this.contract.createFund(ethers.utils.parseEther(`${goalAmount}`), message, groupId, {value: ethers.utils.parseEther(`${startAmount}`)});
                await status.wait();
            } catch(err){
                throw err.data.message;
            }
        }
    }

    async withdraw(groupId){
        await requestAccount();
        this.#connectSigner();

        if (typeof window.ethereum !== 'undefined') {
            try{
                const status = await this.contract.withdrawFund(groupId);
                await status.wait();
            } catch(err){
                throw err;
            }
        }
    }

    async contribute(fundOwner, groupId, amount){
        await requestAccount();
        this.#connectSigner();

        if(amount <= 0 || amount === '') throw('Invalid amount');
        if(typeof window.ethereum !== 'undefined'){
            try{
                const status = await this.contract.contributeToFund(fundOwner, groupId, {value: ethers.utils.parseEther(`${amount}`)});
                await status.wait();
            } catch(err) {
                throw err;
            }
        }
    }

    async getActiveFunds(groupId){
        try{
            var grFunds = await this.contract.getActiveGroupFunds(groupId);
        } catch(err) {
            console.log(err);
            throw err;
        }
        return grFunds;
    }
}