/* To chnage the way the dropdown menu is displayed set the orientation property to either `left` or `right` */

const template = document.createElement('template');
template.innerHTML = `
<style>
/* Dropdown Button */
.dropbtn {
  color: white;
  border: none;
  cursor: pointer;
  height: 100%;
  display: grid;
  align-items: center;
  user-select: none;
}

/* The container <div> - needed to position the dropdown content */
.dropdown {
  position: relative;
  display: inline-block;
  height: 100%;
}

/* Dropdown Content (Hidden by Default) */
.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f1f1f1;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
  width: fit-content;
  margin-top: 0.3rem;
}

/* Links inside the dropdown */
.dropdown-content > * {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

.dropdown-items{
    display: grid;
}

/* Change color of dropdown links on hover */
.dropdown-items *:hover {background-color: #ddd}

/* Show the dropdown menu (use JS to add this class to the .dropdown-content container when the user clicks on the dropdown button) */
.show {display:block;}
</style>

<div class="dropdown">
    <div class="dropbtn" part="drop-btn">
        <slot name="dropdown-label"></slot>
    </div>
    <div id="myDropdown" class="dropdown-content">
        <div class="dropdown-items">
            <slot name="items"></slot>
        </div>
    </div>
</div>
`;

export class dropdown extends HTMLElement {
    constructor() {
        super();

        const shadowRoot = this.attachShadow({ mode: 'closed' });
        // clone template content nodes to the shadow DOM
        shadowRoot.appendChild(template.content.cloneNode(true));
        this.dropContent = shadowRoot.querySelector('.dropdown-content');
        this.btn = shadowRoot.querySelector('.dropbtn');
        shadowRoot.querySelector('.dropbtn').addEventListener('click', () => {
            shadowRoot.getElementById("myDropdown").classList.toggle("show");
        }); 
    }

    static get observedAttributes() {
        return [ 'orientation' ];
    }

    connectedCallback() {
        if (!this.orientation) {
            this.orientation = 'right'; //default
        }
    }

    get orientation() {
        // be careful: attributes always string, if you want a number, you must parse it on your own. 
        return this.getAttribute('orientation');
    }
    
    set orientation(value) {
        if(value !== 'left' || value !== 'right') return;
        // if you set the property maxRating in this class, you must sync them with the attribute
        this.setAttribute('orientation', value);
    }

    attributeChangedCallback(name, oldVal, newVal) {
        if (oldVal !== newVal) {
            switch(name){
                case 'orientation':
                {
                    this.orientation = newVal;
                    if(this.orientation === 'left')
                    {
                        this.dropContent.style.right = '0';
                    }
                    else
                    {
                        this.dropContent.style.removeProperty('right');
                    }
                    break;
                }
            }
        }
    }
}
window.customElements.define('dropdown-menu', dropdown);