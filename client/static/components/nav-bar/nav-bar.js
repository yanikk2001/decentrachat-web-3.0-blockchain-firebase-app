/* 
    NAVIGATION BAR WEB COMPONENT - MOBILE FRIENDLY
    adjustable properties:
    gap-size: num ---- sets gap size between menu elements (it is in %), works both on mobile and big screens
*/

const template = document.createElement('template');
template.innerHTML = `
<style>
/* default style */
:host {
    color: black;
}

/* style for slotted tags from outside  */
::slotted(a) {
    text-decoration: none;
}

@keyframes slideup {
    0% {opacity: 0; transform: translateY(50%);}
    100% {opacity: 1; transform: translateY(0);}
}

@keyframes slideright {
    0% {opacity: 0; transform: translateX(-50%);}
    100% {opacity: 1; transform: translateX(0);}
}

.nav-bar{
    width: 100%;
    display: inline-grid;
    align-items: center;
    grid-template-columns: 30% auto;
    justify-items: center;
    grid-auto-flow: column;
    justify-content: start;
    box-shadow: 0px 0px 3px black;
    position: relative;
    z-index: 99;
}

.nav-tabs{
    white-space: nowrap;
    display: grid;
    align-items: center;
    grid-auto-flow: column;
    column-gap: 30%;
    position: relative;
    animation: slideup 0.9s ease-in-out;
}

slot[name="list-icon"]{
    display: none;
}

@media screen and (max-width: 750px)
{
    slot[name="list-icon"]{
        display: block;
    }

    .nav-bar{
        grid-template-columns: 1fr;
        justify-items: start;
        grid-auto-flow: row;
    }

    .nav-sidebar-wrapper{
    }

    .nav-sidebar{
        display: none;
        animation: slideright 0.9s ease-in-out;
        padding: 8%;
        position: absolute;
        z-index: 1;
        background: green;
        height: 100vh;
        width: fit-content;
    }

    .nav-tabs{
        display: grid;
        grid-auto-flow: row;
        position: relative;
        animation: slideright 0.9s ease-in-out;
        grid-row-gap: 30%;
    }

    .logo-wrapper{
        justify-self: left;
    }

    .nav-logo{
        display: grid;
        grid-template-columns: 15% auto;
        width: 100%;
        justify-items: center;
        align-items: center;
    }
    
}
</style>
    <nav class="nav-bar" part="nav-main">
        <div class="nav-logo">
            <slot name="list-icon">
                <img src="./components/nav-bar/list.svg" alt="">
            </slot>
            <div class="logo-wrapper">
                <slot name="logo"></slot>
            </div>
        </div>
        <div class="nav-sidebar-wrapper">
            <aside class="nav-sidebar" part="nav-sidebar">
                <div class="nav-tabs">
                    <slot name="tabs"></slot>
                </div>
            </aside>
        </div>
    </nav>
`;

export class navBar extends HTMLElement {
    constructor() {
        super();

        const shadowRoot = this.attachShadow({mode: 'closed'});
        // clone template content nodes to the shadow DOM
        shadowRoot.appendChild(template.content.cloneNode(true));
        this.tabs = shadowRoot.querySelector('.nav-tabs');
        shadowRoot.querySelector('slot[name="list-icon"]').addEventListener('click', () => {
            let sideBar = shadowRoot.querySelector('.nav-sidebar');
            let visible = (sideBar.style.display == 'none') ? false : true;
            if(visible)
            {
                sideBar.style.display = 'none';
            }
            else
            {
                sideBar.style.display = 'block';
            }
        });
    }

    static get observedAttributes() {
        return [ 'gap-size' ];
    }

    connectedCallback() {
        if (!this.gapSize) {
            this.gapSize = 30; //default
        }
    }

    get gapSize() {
        // be careful: attributes always string, if you want a number, you must parse it on your own. 
        return +this.getAttribute('gap-size');
    }
    
    set gapSize(value) {
        // if you set the property maxRating in this class, you must sync them with the attribute
        this.setAttribute('gap-size', value);
    }

    attributeChangedCallback(name, oldVal, newVal) {
        if (oldVal !== newVal) {
            this.gapSize = newVal;
            if(window.innerWidth <= 500)
            {
                this.tabs.style.rowGap = `${this.gapSize}%`;
            }
            else
            {
                this.tabs.style.columnGap = `${this.gapSize}%`;
            }
        }
        
    }
}
window.customElements.define('nav-bar', navBar);