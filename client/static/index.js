import { getAuth, onAuthStateChanged } from "firebase/auth";
import { loadHtml } from "./loadHtml.js";
import { walletIsConnected, requestAccount, getActiveAddress, shortenAddress } from "./contract.js";

const DEFALT_PAGE = 'home.html';
let navBar = document.getElementById('navBar');
const myEvents = ['home.html', 'home.html-loaded'];

document.addEventListener('DOMContentLoaded', function(){
    loadHtml(DEFALT_PAGE); //Default page

    //If home.html is activate hover effect
    myEvents.forEach(event => document.addEventListener(event, () => {
        cardHover('.card');
        document.querySelector('body').style.overflow = 'auto';
        document.querySelector('html').style.backgroundColor = '#3E065F';
        getEthStats();
        
        updateCardAddress();
    }));

    addConnectWalletBtn();

    updateLoggedInStatus();

    addListenersNavBar();
});

async function updateCardAddress(){
    document.getElementById('card-address').innerHTML = shortenAddress(await getActiveAddress());
}

async function addConnectWalletBtn(){
    if(await walletIsConnected()) return;

    let btn = document.createElement('div');
    btn.classList = 'connect-btn';
    btn.slot = 'tabs';
    btn.innerHTML = '<a>Connect Wallet</a>';
    btn.addEventListener('click', async () => {
        await requestAccount();
        //hide btn when users connects wallet
        if(window.ethereum.selectedAddress !== null){
            btn.style.display = 'none';
            updateCardAddress();
        }
    });
    navBar.append(btn);
}

function cardHover(cardName){
    const card = document.querySelector(cardName);
      // to compute the center of the card retrieve its coordinates and dimensions
    const {
      x, y, width, height,
    } = card.getBoundingClientRect();
    var cx = x + width / 2;
    var cy = y + height / 2;

    // following the mousemove event compute the distance betwen the cursor and the center of the card
    function handleMove(e) {
      const { pageX, pageY } = e;
    
      // ! consider the relative distance in the [-1, 1] range
      const dx = (cx - pageX) / (width / 2);
      const dy = (cy - pageY) / (height / 2);
    
      // rotate the card around the x axis, according to the vertical distance, and around the y acis, according to the horizontal gap 
      this.style.transform = `rotateX(${10 * dy * -1}deg) rotateY(${10 * dx}deg)`;
    }
    
    // following the mouseout event reset the transform property
    function handleOut() {
      this.style.transform = 'initial';
    }
    card.addEventListener('mousemove', handleMove);
    card.addEventListener('mouseout', handleOut);
}

function createLoginBtn()
{
    let btn = document.createElement('div');
    btn.classList = 'connect-btn login-btn';
    btn.slot = 'tabs';
    btn.id = 'LogInBtn'
    btn.setAttribute('data-page', "log-in.html");
    btn.innerHTML = '<a>Log in here</a>';
    btn.addEventListener('click', () => loadHtml('log-in.html'));
    return btn;
}

function createLogoutBtn()
{
    let btn = document.createElement('div');
    btn.classList = 'connect-btn login-btn';
    btn.slot = 'tabs';
    btn.id = 'LogOutBtn'
    btn.innerHTML = '<a>Log out</a>';
    btn.addEventListener('click', () => {
        getAuth().signOut().then(function() {
            console.log('Signed Out');
        }, function(error) {
            console.error('Sign Out Error', error);
        });
    });
    return btn;
}

function addListenersNavBar()
{
    // let navBar = document.getElementById('navBar');
    let elements = navBar.getElementsByTagName('*');
    for(const element of elements)
    {
        let atrVal = element.getAttribute('data-page'); //returns null if tag doesn`t have that attribute
        if(atrVal)
        {
            element.addEventListener('click', () => loadHtml(atrVal));
        }
    }
}

function updateLoggedInStatus()
{
    const auth = getAuth();
    let loggStatus = document.getElementById('logged-status');
    // let navBar = document.querySelector('#navBar');
    onAuthStateChanged(auth, (user) => { //wait for firebase to connect to servers then get user
        if(!user)
        {
            let lgbtn = document.querySelector('#LogOutBtn')
            if(lgbtn) lgbtn.remove();
            if(!navBar.querySelector('#LogInBtn')) navBar.append(createLoginBtn());
            loggStatus.innerHTML = 'Not Logged-in';
        }
        else
        {
            let lgbtn = document.querySelector('#LogInBtn')//if logged in remove login btn
            if(lgbtn) lgbtn.remove();
            if(!navBar.querySelector('#LogOutBtn')) navBar.append(createLogoutBtn());
            loggStatus.innerHTML = user.email;
        }
    });
}

async function getEthStats()
{
    const response = await fetch('https://api.coingecko.com/api/v3/simple/price?ids=ethereum&vs_currencies=usd&include_market_cap=true');
    const gasResponse = await fetch('https://ethgasstation.info/json/ethgasAPI.json');
    const result = await response.json();
    const gasResult = await gasResponse.json();
    if(response.ok && gasResponse.ok)
    {
        const eth = result.ethereum;
        document.getElementById('info-price').innerHTML = Math.round(eth.usd) + '$';
        document.getElementById('info-mktCap').innerHTML = Math.round(eth.usd_market_cap) + '$';
        document.getElementById('info-gasPrice').innerHTML = gasResult.average/10 + ' Gwei';
    }
}








