import { getDatabase, ref, set, push, child, update, onValue, remove, onChildAdded, onChildChanged, onChildRemoved, query, limitToLast, off } from "firebase/database";
import { firebaseHelper } from "./firebaseHelper.js";
import { FundManager, walletIsConnected, shortenAddress } from "./contract.js";
import { showAlert } from "./alert.js";
import { ethers } from "ethers";

const DEFAULT_GROUP_IMG = '../media/people.svg';
const DEFAULT_PERSON_IMG = '../media/person.svg';
const LIKE_ICON = '../media/like.svg';
const DISLIKE_ICON = '../media/dislike.svg';
const TRASH_ICON = '../media/trash.svg';

const db = getDatabase();

let chatGroups;
let messageRooms;
let chatForm;
let chatInput;
let createFundBtn;
let ChatSendBtn;
let chat;

let header_title;
let header_img;
let header_members;
let header_groupId;

let fundManager;

let chatRoom;

class fundUIComp{
    title;
    owner;
    status;
    treasury;
    goal;
    contributors;
    contributeBtn;
    withdrawBtn;
    otherFundsBtn;
    input;

    show_witdhdraw(){
        this.withdrawBtn.style.display = 'block';
    }

    hide_withdraw(){
        this.withdrawBtn.style.display = 'none';
    }
}

let fundComp = new fundUIComp;

document.addEventListener('chat.html', () => {
    //Get all references on load
    header_title = document.querySelector('#chat-header-title');
    header_img = document.querySelector('#chat-header-img');
    header_members = document.querySelector('#chat-header-members');
    header_groupId = document.querySelector('#chat-header-groupId');

    messageRooms = document.querySelector('#chat-rooms');
    chatForm = document.querySelector('#chat-input-form');
    chatInput = document.querySelector('#chat-input-text');
    createFundBtn = document.querySelector('#chat-create-fund-btn');
    ChatSendBtn = document.querySelector('#chat-send-btn');
    chat = document.querySelector('.chat');

    fundComp.title = document.getElementById('fund-message');
    fundComp.owner = document.getElementById('fund-owner');
    fundComp.status = document.getElementById('fund-status');
    fundComp.treasury = document.getElementById('fund-treasury');
    fundComp.goal = document.getElementById('fund-goalAmount');
    fundComp.contributors = document.getElementById('fund-contributors');
    fundComp.contributeBtn = document.getElementById('fund-contributeBtn');
    fundComp.withdrawBtn = document.getElementById('fund-withdrawBtn');
    fundComp.otherFundsBtn = document.getElementById('fund-otherFunds');
    fundComp.input = document.getElementById('fund-contr-input');

    chatRoom = new ChatRoom;

    document.querySelector('body').style.overflow = 'hidden';
    document.querySelector('html').style.backgroundColor = '#fff';

    let btn = document.querySelector('#chat-add-new-btn'); //Event listener for new group btn
    btn.addEventListener('click', () => {
        addCreateJoinChatForm();
        let img = btn.querySelector('img').src;
        let title = btn.querySelector('span').innerHTML;
        setHeader(title, img);
    });

    document.querySelector('#toggle-switch').addEventListener('change', function() {
        if(this.checked) //Join
        {
            showElement('.join-group');
            hideElement('.create-group');
        }
        else //Create
        {
            showElement('.create-group');
            hideElement('.join-group');
        }
    })

    fundManager = new FundManager;
});

//Manages sending, loading messages into chat rooms
class ChatRoom
{
    #activeChatRoom; //Currently viewed room
    #numOfMessagesToLoad = 100;

    constructor()
    {
        this.#loadChatRooms();
        this.#addEventListenerToChatInput();
        this.funds = new fundUI;
        // this.#topOfChatListener();
    }

    get activeChatRoom(){
        return this.#activeChatRoom;
    }

    set activeChatRoom(value){
        this.#activeChatRoom = value;
    }

    static joinNew(key)
    {
        firebaseHelper.getUser()
        .then(user => {
            // Update user rooms
            const userRoomRef = ref(db, 'users/' + user.uid + '/rooms/' + key);
            set(userRoomRef, true);

            // Update room`s members
            const membersRef = ref(db, 'chatMembers/' + key);
            const newMemberRef = push(membersRef);
            set(newMemberRef, user.uid);

            // Upadte room`s number of members
            const roomMembersRef = ref(db, 'chatRooms/' + key + '/numberOfMembers');
            firebaseHelper.getData(roomMembersRef)
            .then(memberNum => {set(roomMembersRef, ++memberNum)})
            .catch(err => console.log(err));
        })
        .catch(err => alert(err));
    }

    static createNew(name, password=null)
    {
        const newChatKey = push(child(ref(db), 'chatRooms')).key;
        const room = {
            name: name,
            numberOfMembers: 1,
            password: password,
        }

        firebaseHelper.getUser()
        .then(user => {
            const updates = {};
            updates['/chatRooms/' + newChatKey] = room;
            updates['/users/' + user.uid + '/rooms/' + newChatKey] = true;
            const MembersRef = ref(db, 'chatMembers/' + newChatKey);
            const newMemberRef = push(MembersRef);
            set(newMemberRef, user.uid);
            return update(ref(db), updates);
        })
        .catch(err => alert(err));
    }

    #loadChatRooms()
    {
        firebaseHelper.getUser()
        .then(user => {
            const userChatRoomsRef = ref(db, 'users/' + user.uid + '/rooms');
            off(userChatRoomsRef); //Remove previous event listeners if there were any
            onValue(userChatRoomsRef, (snapshot) => {
                console.log("LOADING GROUPS------------------------");

                chatGroups = document.querySelector('.chat-groups');
                chatGroups.innerHTML = ''; //clear old contents

                const data = snapshot.val();
                for(const groupID in data)
                {
                    const chatRoomsRef = ref(db, 'chatRooms/' + groupID);
                    firebaseHelper.getData(chatRoomsRef)
                    .then(group => this.#addChat(group.name, groupID))
                    .catch(err => console.log(err));
                }
            });
        })
        .catch(err => alert('You need to be logged in to chat'));
    }

    #addChat(name, id, img=DEFAULT_GROUP_IMG)
    {
        let chatEl = document.createElement('a');
        chatEl.classList = 'chat-box chat-group';
        chatEl.setAttribute('group-id', id);
        chatEl.innerHTML = `<img src="${img}" alt="plus">
                            <span class="chatName">${name}</span>`;

        let messageRoom = document.createElement('div');
        messageRoom.id = id;
        messageRoom.classList = "messages-grid";
        messageRooms.append(messageRoom);

        const messagesRef = query(ref(db, 'messages/' + id), limitToLast(this.#numOfMessagesToLoad)); //Get only most recent 20 messages
        this.#loadMessages(messagesRef, messageRoom, id);
        
        chatEl.addEventListener('click', () => { //When chat tab is clicked
            setHeader(name, img);
            const chatRef = ref(db, 'chatRooms/' + id);
            setHeaderMembers(chatRef);
            setHeaderGroupId(id);

            showElement('.chat-input', 'grid');
            show_view(id, 'grid');
            chat.scrollTop = chat.scrollHeight; //Scroll to bottom
            this.activeChatRoom = id;
            console.log(this.activeChatRoom);

            this.funds.displayActiveFundCampaigns();
        });

        chatGroups.append(chatEl);
    }

    #loadMessages(messagesRef, messageRoom, chatId)
    {
        off(messagesRef);
        onChildAdded(messagesRef, (message) => { //Add listener that loads all messages and then updates when new message is sent
            this.#addMessageBubble(messageRoom, message.val(), message.key, chatId);
        });

        onChildRemoved(messagesRef, (message) => {
            messageRoom.innerHTML = '';
            this.#loadMessages(messagesRef, messageRoom, chatId);
        });
    }

    //When clicked updates corresponding field in the database
    #addReactionOnClickEvent(element, reaction, groupId, messageId)
    {
        element.addEventListener('click', () => {
            const messageRef = ref(db, 'messages/' + groupId + '/' + messageId + '/' + reaction);
            firebaseHelper.getData(messageRef)
            .then(reactionNum => set(messageRef, ++reactionNum))
            .catch(err => console.log(err));
        });
    }

    //When it detects change - updates the corresponding view number
    #addReactionListener(element, groupId, messageId)
    {
        const messageRef = ref(db, 'messages/' + groupId + '/' + messageId);
        onChildChanged(messageRef, (data) => {
            const reactionChanged = data.ref._path.pieces_[3]; //Either likes or dislikes
            element.querySelector('.'+reactionChanged).innerHTML = data.val();
            element.style.display = 'grid';
        });
    }

    #deleteMessage(groupdId, messageId)
    {
        console.log("removed message");

        const messageRef = ref(db, 'messages/' + groupdId + '/' + messageId);
        remove(messageRef);
    }

    #addMessageBubble(parentElement, message, messageKey, chatId)
    {
        let bubble = document.createElement('div');
        bubble.id = "message-bubble";

        let userProfile = document.createElement('div');
        userProfile.innerHTML = `<img src="${DEFAULT_PERSON_IMG}" alt="User profile image">`;

        let messageReactions = document.createElement('div');
        messageReactions.classList = 'message-reactions';
        if(message.likes == 0 && message.dislikes == 0)
            messageReactions.style.display = 'none'; //Hide reaction count if there are no reactions

        let MessageLikes = document.createElement('div');
        MessageLikes.classList = 'reaction-box';
        MessageLikes.innerHTML = `
            <img src="${LIKE_ICON}" alt="Likes">
            <span class="likes">${message.likes}</span>
        `;
        this.#addReactionOnClickEvent(MessageLikes, 'likes', chatId, messageKey);
        messageReactions.append(MessageLikes);

        let MessageDislikes = document.createElement('div');
        MessageDislikes.classList = 'reaction-box';
        MessageDislikes.innerHTML = `
            <img src="${DISLIKE_ICON}" alt="Dislikes">
            <span class="dislikes">${message.dislikes}</span>
        `;
        this.#addReactionOnClickEvent(MessageDislikes, 'dislikes', chatId, messageKey);
        messageReactions.append(MessageDislikes);

        this.#addReactionListener(messageReactions, chatId, messageKey);

        
        firebaseHelper.getUser()
        .then(user => {
            let isMessageAuthor = user.email === message.sender;

            let Message = document.createElement('div');
            Message.id = "message-info";
            Message.innerHTML = `
            <h3>${message.text}</h3>
            <h6>- ${message.sender}</h6>
            <h6>on ${message.timestamp}</h6>
            `;
            if(isMessageAuthor) {
                Message.classList = "gradient-card-var3"
                userProfile.id = "message-profile";
            }
            else {
                Message.classList = "gradient-card-var2"; 
                userProfile.id = "message-profile-other";
                MessageLikes.classList = 'reaction-box-other';
                MessageDislikes.classList = 'reaction-box-other';
            }

            if(isMessageAuthor)
            {
                var messageDeleteBtn = document.createElement('div');
                messageDeleteBtn.classList = 'message-delete-btn'
                messageDeleteBtn.innerHTML = `
                    <img src="${TRASH_ICON}" alt="delete">
                `;
                messageDeleteBtn.style.display = 'none';
        
                messageDeleteBtn.addEventListener('click', () => this.#deleteMessage(chatId, messageKey));
            }

            Message.addEventListener('click', () => {
                if(messageReactions.style.display == 'none')
                    messageReactions.style.display = 'grid';
                else
                    messageReactions.style.display = 'none';

                if(isMessageAuthor)
                {
                    if(messageDeleteBtn.style.display == 'none' && messageReactions.style.display == 'grid')
                    {
                        messageDeleteBtn.style.display = 'grid';
                        messageReactions.style.display = 'grid';
                    }
                    else
                        messageDeleteBtn.style.display = 'none';
                }
            });
            
            bubble.append(userProfile);
            bubble.append(Message);
            if(isMessageAuthor) bubble.append(messageDeleteBtn);
            bubble.append(messageReactions);
            parentElement.append(bubble);
        })
        .catch(err => alert(err));
    }
    
    #addEventListenerToChatInput()
    {
        chatForm.onsubmit = () => {
            this.#sendMessage();
            return false;
        }

        ChatSendBtn.addEventListener('click', () => {this.#sendMessage()});
    }

    #sendMessage()
    {
        firebaseHelper.getUser()
        .then(user => {
            if(chatInput.value === '') return;

            const message = {
                sender: user.email,
                text: chatInput.value,
                timestamp: getCurrDate(),
                likes: 0,
                dislikes: 0
            }

            const messagesRef = ref(db, "messages/" + this.activeChatRoom);
            const newMessageRef = push(messagesRef);
            
            try{
                console.log("message sent");
                set(newMessageRef, message);
            }
            catch(err){
                alert(err);
            };
            chatInput.value = '';
            chat.scrollTop = chat.scrollHeight; //Scroll to bottom
        })
        .catch(err => alert(err));
    }

    //Loads more past messages when at the top of chat
    // #topOfChatListener()
    // {
    //     chat.onscroll = () => {
    //         if (chat.scrollTop <= 0) {
    //             const messagesRef = query(ref(db, 'messages/' + this.activeChatRoom), limitToLast(this.activeChatRoom));

    //             get(messagesRef)
    //             .then((snapshot) => {
    //                 if (snapshot.exists()) 
    //                 {
    //                     let messages = snapshot.val();
    //                     console.log(messages);
    //                 } 
    //                 else 
    //                 {
    //                     console.log("Failed to load next messages");
    //                 }
    //             })
    //             .catch((err) => console.log(err));
    //         }
    //     };
    // }
}

function getCurrDate()
{
    var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = time+' '+date;

    return dateTime;
}

function setHeader(title, img)
{
    header_title.innerHTML = title;
    header_img.src = img;
}

function setHeaderMembers(chatRef)
{
    off(chatRef);

    const membersRef = child(chatRef, 'numberOfMembers');

    // Get only once the members when loading
    firebaseHelper.getData(membersRef)
    .then(data => {
        header_members.innerHTML = 'Members: ' + data;
    })
    .catch(err => {header_members.innerHTML = ''});

    // After new members join update count automatically
    onChildChanged(chatRef, (memberNum) => {
        header_members.innerHTML = 'Members: ' + memberNum.val();
    })
}

function setHeaderGroupId(id)
{
    header_groupId.innerHTML = id;
}

function addCreateJoinChatForm()
{
    show_view('join-create-menu');
    hideElement('.chat-input');

    document.querySelector('#create-form').onsubmit = () => {
        const name = document.getElementById('chat-name').value;
        const password = document.getElementById('chat-password').value;
        if(name == ''){
            showAlert('Name field is required');
            return false;
        }
        if(password == '') ChatRoom.createNew(name);
        else ChatRoom.createNew(name, password);
    
        return false;
    };

    document.querySelector('#join-form').onsubmit = () => {
        const key = document.querySelector('#chat-key').value;
        ChatRoom.joinNew(key);
        return false;
    };
}

function show_view(viewId, displayType='block')
{
    var container = document.getElementById(viewId).parentElement;
    var views = container.children;
    for(var i = 0; i < views.length; i++)
    {
        if(views[i].id == viewId)
        {
            views[i].style.display = displayType;
        }
        else
        {
            views[i].style.display = 'none';
        }
    }
}

function hideElement(element)
{
    document.querySelector(element).style.display = 'none';
}

function showElement(element, displayType='block')
{
    document.querySelector(element).style.display = displayType;
}

class fundUI{
    constructor(){
        createFundBtn.addEventListener('click', () => {
            show_view('fund-input');
            hideElement('.chat-input');

            document.querySelector('#fund-form').onsubmit = () => {
                const title = document.getElementById('fund-title').value;
                const goal = document.getElementById('fund-goal').value;
                const strAMount = document.getElementById('fund-str-amount').value;
            
                fundManager.startFundCampaign(goal, title, chatRoom.activeChatRoom, strAMount).catch(err => {
                    console.log(err);
                    showAlert(err);
                });
                return false;
            };
        });

        //Add event listeners to fund buttons
        fundComp.contributeBtn.addEventListener('click', function(){
            let amount = fundComp.input.value;
            let fundOwner = this.getAttribute('owner');
            fundManager.contribute(fundOwner, chatRoom.activeChatRoom, amount).catch(err => {
                alert(err);
            });
            fundComp.input.value = '';
        });

        fundComp.withdrawBtn.addEventListener('click', () => {
            fundManager.withdraw(chatRoom.activeChatRoom).catch(err => alert(err));
        });

        const listenedEvents = ['contributedFundEvent', 'createFundEvent', 'fundFundedEvent'];

        //Add event listeners when new changes happen to group`s funds -- refresh displayed info
        listenedEvents.forEach(event => document.addEventListener(event, () => this.displayActiveFundCampaigns()));
    }

    async displayActiveFundCampaigns() {
        if(typeof(chatRoom.activeChatRoom) === 'undefined') return;
        let fundCampaigns = await fundManager.getActiveFunds(chatRoom.activeChatRoom);
        if(fundCampaigns.length === 0) {
            hideElement('.chat-funds');
            return;
        }

        showElement('.chat-funds', 'grid');
        this.#updateFundHeaderBar(fundCampaigns[0]); //Display by default first result

        //Add all campaigns to a dropdown list
        fundComp.otherFundsBtn.innerHTML = '<span slot="dropdown-label">▼</span>';
        for(let i = 0; i < fundCampaigns.length; i++)
        {
            let el = document.createElement('div');
            el.classList = "other-funds";
            el.slot = 'items';
            el.innerHTML = `${fundCampaigns[i].message} By: ${shortenAddress(fundCampaigns[i].owner)} ${this.#calcPercentCompleted(fundCampaigns[i])}%`;
            //When clicked display its info
            el.addEventListener('click', () => {
                this.#updateFundHeaderBar(fundCampaigns[i]);
            }); 
            fundComp.otherFundsBtn.append(el);
        }
    }

    async #updateFundHeaderBar(fundCampaign){
        fundComp.title.innerHTML = fundCampaign.message;
        fundComp.owner.innerHTML = shortenAddress(fundCampaign.owner);
        fundComp.status.innerHTML = this.#calcPercentCompleted(fundCampaign);
        fundComp.treasury.innerHTML = ethers.utils.formatEther(fundCampaign.treasury.toString());
        fundComp.goal.innerHTML = ethers.utils.formatEther(fundCampaign.goalAmount.toString());
        fundComp.contributors.innerHTML = '';
        fundCampaign.contributors.forEach(contr => {
            let newContr = document.createElement('li');
            newContr.innerHTML = `${shortenAddress(contr)}`;
            fundComp.contributors.append(newContr);
        });
        fundComp.contributeBtn.setAttribute('owner', fundCampaign.owner); //Upadtes to which fund to contribute if clicked
        
        if(await walletIsConnected()){
            //Only show withdraw btn if user is the owner of the fund
            fundManager.getSigner().then(res => {
                if(res === fundCampaign.owner) fundComp.show_witdhdraw()
                else fundComp.hide_withdraw();
            });
        }
    }

    #calcPercentCompleted(fundCampaign){
        return fundCampaign.treasury / fundCampaign.goalAmount * 100;
    }
}