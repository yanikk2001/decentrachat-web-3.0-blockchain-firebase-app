import { getAuth, onAuthStateChanged } from "firebase/auth";
import { get } from "firebase/database";

export class firebaseHelper
{
    static getData(ref)
    {
        return new Promise((resolve, reject) => {
            get(ref)
            .then((snapshot) => {
                if (snapshot.exists()) 
                {
                    resolve(snapshot.val());
                } 
                else 
                {
                    reject('No data available');
                }
            })
            .catch((err) => {
                reject(err);
            });
        })
    }

    static getUser()
    {
        const auth = getAuth();
        return new Promise((resolve, reject) => {
            onAuthStateChanged(auth, (user) => {
                if(!user) {
                    reject('You need to be logged in to perform that action');
                }
                else{
                    resolve(user);
                }
            });
        });
    }
}