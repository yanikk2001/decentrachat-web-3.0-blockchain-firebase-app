# DecentraChat 🐱‍🚀
## Web 3.0 Messaging and Fund raising platform

![home img](./ReadMeFiles/home.png)

DecentraChat is WEB 3.0 powered messaging app using the realtime firebase database and authentication. 

- Create groups 💭
- exchange messages 💌
- send ether 🤑
Across the world 🌍

## Features

### Participate in chat groups 🐱‍🏍
Enter or create the next hub for sharing great ideas among your friends 

![chat img](./ReadMeFiles/chat.png)
- All messages update in real-time across all clients
- React to individual messages
- Delete your unwanted messages anytime by clicking on them to show the delete btn
![del img](./ReadMeFiles/del.png)

### Create fund campaigns 🎉
![fund img](./ReadMeFiles/fund.png)

Start or contribute to your group`s active fund campaigns with ETH. Raise funds in a safe decentralised way using the blockchain.

- Start fund campaigns and set goal amount. When it is reached all collected funds are automatically transfered to your wallet (the fund`s creator wallet).
- At any time you can stop the funding campaign and withdraw all funds that were raised. 
- Contribute to as many campaigns as you like.

![fund1 img](./ReadMeFiles/fund1.png)
 
> Note: Only `10` active fund campaigns are allowed per chat!

## Implementation 💻
During the development process design flexibility and responsive layout were a main priority.
### Custom Web components
For this project specifically 2 responsive web components were made.
> They are in the `components` directory inside `client` for those interested. 👏

#### Navigation bar
```sh
<nav-bar gap-size=(some value)>
            <element slot="logo">DecentraChat</element>
            <element  slot="tabs">Tab 1</element>
            <element  slot="tabs">Tab 2</element>
</nav-bar>
```

Mobile view - the navigation bar changes according to the screen size. Some of its properties like gap size between buttons can be set as html properties.
![mobile img](./ReadMeFiles/mobile.png)
![mobile2 img](./ReadMeFiles/mobile2.png)

#### Drop down menu
```sh
<dropdown-menu orientation="left">
    <div slot="dropdown-label">
        Some label here
    </div>
    <div slot="items">
        This is the drop drown content
    </div>
</dropdown-menu>
```

## Architecture 🏠
The whole platform architecture follows the "single page design concept".
`Index.html` serves as the entry point inside of which all other html pages are loaded when selected from the nav bar. Once loaded the html pages are cached into session storage to save bandwidth. These loading and caching processes are controlled by the `loadHtml` function inside `loadHtml.js`. When loading certain page an event is dispatched named afer the file that is being loaded. This is used by different sections of the platform like `index.js` and `chat.js` to know when to do certain actions related to the specific page that was loaded.

> Example: When home.html event is listened to index.js fetches the most recent ethereum market stats and displays them


## Tech

DecentraChat uses a number of open source projects to work properly:

- [Firebase](https://firebase.google.com/) - Real-time database and authentication system
- [Hardhat](https://hardhat.org/) - Smart contarct framework and testing ground
- [Ethers js](https://docs.ethers.io/v5/) - Blockchain Eth api

And of course DecentraChat itself is open source with a [public repository](https://gitlab.com/yanikk2001/decentrachat-web-3.0-blockchain-firebase-app)
 on GitLab.

## Installation 👩‍💻
### For development environment
DecentraChat requires [Npm](https://www.npmjs.com/) to run.

Install the dependencies and devDependencies and start a local server.

```sh
cd client
npm run build
npm start
```

To start a local hardhat node (Required for the fund campaign functionality)

```sh
cd smart_contract
npx hardhat node
```
To compile and lauch contract to the local node
```sh
npx hardhat run scripts/deploy.js --network localhost
```
> Note: It will print the contract's address, copy it and paste it inside `client/static/contract.js` in the `contractAddress` const at the top

## License 📃

MIT

**Free Software, Hell Yeah!**
